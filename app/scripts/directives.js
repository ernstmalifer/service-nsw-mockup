// 'use strict';
var a = '';

angular.module('Servicensw.directives', [])
 
.directive('numpad', function() {
  return {
      restrict: 'AE',
      // replace: 'true',
      templateUrl: 'templates/numpad.html',

      scope: {
	      number: '=number'
	    },

      link: function(scope, elem, attrs) {
        elem.bind('click', function(target) {

      	  targetElement = target.srcElement.attributes[1].value;

      	  if(targetElement == 'back') {
      	  	scope.number = scope.number.substring(0, scope.number.length - 1);
      	  } else if(targetElement == 'clear') {
      	  	scope.number = '';
      	  } else if(targetElement == '1' ||
      	  	        targetElement == '2' ||
      	  	        targetElement == '3' ||
      	  	        targetElement == '4' ||
      	  	        targetElement == '5' ||
      	  	        targetElement == '6' ||
      	  	        targetElement == '7' ||
      	  	        targetElement == '8' ||
      	  	        targetElement == '9' ||
      	  	        targetElement == '0'
      	  	){
      	  	scope.number += targetElement;
      	  }

          scope.$apply(function() {
          });

          // console.log(scope.number);
          scope.$emit('changevalue', scope.number);

        });
      },

  };
})

.directive('keypad', function() {
  return {
      restrict: 'AE',
      // replace: 'true',
      templateUrl: 'templates/keypad.html',

      scope: {
        text: '=text',
        inputname: '=inputname'
      },

      link: function(scope, elem, attrs) {
        elem.bind('click', function(target) {

          targetElement = target.srcElement.attributes[1].value;

          if(targetElement == "exit") {
            scope.$emit('exit', true);
          } else if(targetElement == "enter") {
            scope.$emit('exit', true);
          } else {
            if(targetElement == 'back') {
              scope.text = scope.text.substring(0, scope.text.length - 1);
            } else if(targetElement == 'clear') {
              scope.text = '';
            } else if(targetElement == 'uppercase') {
            } else if(targetElement == 'number') {
            } else if(targetElement == 'enter') {
            } else if(targetElement.length == 1){
              scope.text += targetElement;
            }

            scope.$emit('changevalue', [scope.inputname,scope.text]);

            scope.$apply(function() {
            });
          }
          

        });
      },

  };
});
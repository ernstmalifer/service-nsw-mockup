'use strict';
angular.module('Servicensw.controllers', [])

.controller('NavBarCtrl', function($scope, $location) {
	$scope.isHome = function(){ if($location.path() == '/home') return true; else return false; }
})


.controller('IndexCtrl', function($scope, $sce) {

  $scope.links = [
    { id: 1, title: 'Renew Rego', url: '' },
    { id: 2, title: 'Transfer Rego', url: 'licenceentry' },
    { id: 3, title: 'Maintain Info', url: '' },
    { id: 4, title: 'Set up OnlineAccount', url: '' }
  ];

})

.controller('LicenceEntryCtrl', function($scope) {

  $scope.links = [
    { id: 11, title: 'Swipe licence', url: 'swipelicence' },
    { id: 12, title: 'Enter licence number', url: 'licenceentrykeypad' },
  ];

})

.controller('SwipeLicenceCtrl', function($scope, $timeout, $location) {

	var timer = $timeout(function(){
		$location.path('platenumber');
	}, 3000);

	$scope.$on('$destroy', function() {
		$timeout.cancel( timer );
	});

})

.controller('LicenceEntryKeypadCtrl', function($scope, $location, $ionicModal, $timeout) {
	$scope.number = '';

	// var timer = $timeout(function(){
	// 	$scope.modal.show();
 //    	html2canvas(document.querySelectorAll('.fadecontainer')[0], {
	//         onrendered: function (canvas) {
	//             document.querySelectorAll('#poi')[0].appendChild(canvas);
	//         },
	//         width: 1366,
	//         height: 768
	//     });
	// }, 1000);

	$ionicModal.fromTemplateUrl('templates/modal/poi.html', {
		scope: $scope,
		animation: 'slide-in-up'
		}).then(function(modal) {
		$scope.modal = modal;
	});

	$scope.closeModal = function() {
		$scope.modal.hide();
	};

	$scope.$on('$destroy', function() {
		$scope.modal.remove();
	});

	$scope.$on('modal.hidden', function() {
	});

	$scope.$on('modal.removed', function() {
	});

    $scope.$on('changevalue', function (evt, value) {

        if(value.length == 6){
        	$scope.modal.show();
        	html2canvas(document.querySelectorAll('.fadecontainer')[0], {
		        onrendered: function (canvas) {
		            document.querySelectorAll('#poi')[0].appendChild(canvas);
		        },
		        width: 1366,
		        height: 768
		    });
        }
    });
})

.controller('POICtrl', function($scope, $location) {
	
	$scope.number = '';

	$scope.closeModal = function(){
		$scope.modal.hide();
	};

    $scope.$on('changevalue', function (evt, value) {

        if(value.length == 6){
        	$scope.modal.hide();
        	$location.path('platenumber');
        }
    });

})

.controller('PlateNumberCtrl', function($rootScope, $scope, $location, $ionicScrollDelegate) {
	$rootScope.platenumber = '';
	$rootScope.haskeypad = true;

    $scope.showAndSetKeypad = function(inputname, input, scroll){
		$rootScope.haskeypad = true;
		$scope.inputname = inputname;
		$scope.input = input;

		if(scroll){
			$ionicScrollDelegate.scrollTo(0, scroll, true);
		}
	}

	$scope.toggleKeypad = function(){
		$rootScope.haskeypad = !$rootScope.haskeypad;
		
		if($rootScope.haskeypad){
			$ionicScrollDelegate.scrollBottom(true);
		} else {
			$ionicScrollDelegate.scrollTop(true);
		}
	}

	$scope.$on('changevalue', function (evt, value) {
		// if(value[1].length == 6){
        // 	$scope.continue();
        // }
        $rootScope.platenumber = value[1];
    });

    $scope.$on('exit', function (evt, value) {
        $scope.toggleKeypad();
    });

    $scope.continue = function(){
    	$location.path('registerto');
    }
})

.controller('KeypadCtrl', function($scope) {
	$scope.uppercase = true;
})

.controller('RegisterToCtrl', function($rootScope, $scope, $location, _) {

  $scope.tos = [
    { id: 1, title: 'Me', description: 'used substantially for social, pleasure or domestic purposes' },
    { id: 2, title: 'Another Person', description: 'business use unless covered by another usage' },
    { id: 3, title: 'Licenced Motor Dealer', description: 'a motor vehicle which is let out for hire without driver' },
    { id: 4, title: 'A Corporation', description: 'a motor vehicle which is let out for hire without driver' }
  ];

  $scope.select = function(to){
  	_.each($scope.tos, function(toitem){toitem.selected = false;});
  	to.selected = true;
  }


	$scope.continue = function(){
		var selected = _.find($scope.tos, function(toitem){ return toitem.selected; });
		switch(selected.id){
			case 1:
			case 2:
			  $location.path('customerdetails');
			  break;
			case 3:
			  $location.path('dealerdetails');
			  break;
			case 4:
			  $location.path('corporationdetails');
			  break;
			default:
			  $location.path('customerdetails');
			  break;
		}
    }
})

.controller('CustomerDetailsCtrl', function($rootScope, $scope, $location, $ionicScrollDelegate) {

	$rootScope.haskeypad = false;
	$scope.inputname = '';
	$scope.input = {};

	$scope.platenumber = $rootScope.platenumber;
	$scope.customerdetail = {
		surname: 'Foley',
		givenname: 'Seth',
		dob: {
			month: '3',
			day: '02',
			year: '1990'
		},
		email: 'user@email.com',
		mobile: '(02) 6706 8478',
		residential: {
			unit: 'Unit 1',
			number: '51',
			street: 'Oak St'
		},
		mailing: {
			unit: 'Unit 1',
			number: '51',
			street: 'Oak St'
		}
	}

	$scope.toggleKeypad = function(){
		$rootScope.haskeypad = !$rootScope.haskeypad;
		
		if($rootScope.haskeypad){
			$ionicScrollDelegate.scrollBottom(true);
		} else {
			$ionicScrollDelegate.scrollTop(true);
		}
	}

	$scope.showAndSetKeypad = function(inputname, input, scroll){
		$rootScope.haskeypad = true;
		$scope.inputname = inputname;
		$scope.input = input;

		if(scroll){
			$ionicScrollDelegate.scrollTo(0, scroll, true);
		}
	}

	$scope.$on('changevalue', function (evt, value) {
        eval("$scope." + value[0] + " = '" + value[1] + "'");
    });

    $scope.$on('exit', function (evt, value) {
        $scope.toggleKeypad();
    });

	$scope.continue = function(){
    	$location.path('purchaseacquisition');
    }
})

.controller('DealerDetailsCtrl', function($rootScope, $scope, $location, $ionicScrollDelegate) {

	$rootScope.haskeypad = false;
	$scope.inputname = '';
	$scope.input = {};

	$scope.platenumber = $rootScope.platenumber;
	$scope.dealerdetail = {
		name: 'XXXXXXXXXXXXXXXXXXXXXXXXXX'
	}

	$scope.toggleKeypad = function(){
		$rootScope.haskeypad = !$rootScope.haskeypad;
		
		if($rootScope.haskeypad){
			$ionicScrollDelegate.scrollBottom(true);
		} else {
			$ionicScrollDelegate.scrollTop(true);
		}
	}

	$scope.showAndSetKeypad = function(inputname, input, scroll){
		$rootScope.haskeypad = true;
		$scope.inputname = inputname;
		$scope.input = input;

		if(scroll){
			$ionicScrollDelegate.scrollTo(0, scroll, true);
		}
	}

	$scope.$on('changevalue', function (evt, value) {
        eval("$scope." + value[0] + " = '" + value[1] + "'");
    });

    $scope.$on('exit', function (evt, value) {
        $scope.toggleKeypad();
    });

	$scope.continue = function(){
    	$location.path('purchaseacquisition');
    }
})

.controller('CorporationDetailsCtrl', function($rootScope, $scope, $location, $ionicScrollDelegate) {

	$rootScope.haskeypad = false;
	$scope.inputname = '';
	$scope.input = {};

	$scope.platenumber = $rootScope.platenumber;
	$scope.corporationdetail = {
		name: 'Company Corp',
		address: {
			unit: 'Unit 8',
			number: '51',
			street: 'Oak St'
		},
		abn: 'XXXXXXXX',
		acn: 'XXXXXXXX',
		capacity: 'XXXXXXXX'
	}

	$scope.toggleKeypad = function(){
		$rootScope.haskeypad = !$rootScope.haskeypad;
		
		if($rootScope.haskeypad){
			$ionicScrollDelegate.scrollBottom(true);
		} else {
			$ionicScrollDelegate.scrollTop(true);
		}
	}

	$scope.showAndSetKeypad = function(inputname, input, scroll){
		$rootScope.haskeypad = true;
		$scope.inputname = inputname;
		$scope.input = input;

		if(scroll){
			$ionicScrollDelegate.scrollTo(0, scroll, true);
		}
	}

	$scope.$on('changevalue', function (evt, value) {
        eval("$scope." + value[0] + " = '" + value[1] + "'");
    });

    $scope.$on('exit', function (evt, value) {
        $scope.toggleKeypad();
    });

	$scope.continue = function(){
    	$location.path('purchaseacquisition');
    }
})

.controller('PurchaseAcquisitionCtrl', function($rootScope, $scope, $location, $ionicScrollDelegate) {

	$rootScope.haskeypad = false;
	$scope.inputname = '';
	$scope.input = {};

	$scope.platenumber = $rootScope.platenumber;
	$scope.purchaseacquisition = {
		dop: {
			month: '1',
			day: '01',
			year: '2014'
		},
		saleprice: '8888888888',
		garage: {
			unit: 'Unit 8',
			number: '51',
			street: 'Oak St'
		}
	}

	$scope.toggleKeypad = function(){
		$rootScope.haskeypad = !$rootScope.haskeypad;
		
		if($rootScope.haskeypad){
			$ionicScrollDelegate.scrollBottom(true);
		} else {
			$ionicScrollDelegate.scrollTop(true);
		}
	}

	$scope.showAndSetKeypad = function(inputname, input, scroll){
		$rootScope.haskeypad = true;
		$scope.inputname = inputname;
		$scope.input = input;

		if(scroll){
			$ionicScrollDelegate.scrollTo(0, scroll, true);
		}
	}

	$scope.$on('changevalue', function (evt, value) {
        eval("$scope." + value[0] + " = '" + value[1] + "'");
    });

    $scope.$on('exit', function (evt, value) {
        $scope.toggleKeypad();
    });

	$scope.continue = function(){
    	$location.path('typeofuse');
    }
})
.controller('TypeOfUseCtrl', function($scope, _, $location) {

  $scope.uses = [
    { id: 0, title: 'Private', description: 'used substantially for social, pleasure or domestic purposes' },
    { id: 1, title: 'Business', description: 'business use unless covered by another usage' },
    { id: 2, title: 'Rental Car', description: 'a motor vehicle which is let out for hire without driver' },
    { id: 3, title: 'Resale', description: 'second hand vehicles held for resale by licenced motor dealers' },
    { id: 4, title: 'Demonstrator', description: 'demonstrator vehicles used by licenced motor dealers' },
    { id: 5, title: 'Taxi', description: 'used substantially for social, pleasure or domestic purposes' },
    { id: 6, title: 'Long distance, tourist or charter service', description: 'business use unless covered by another usage' },
    { id: 7, title: 'Regular passenger bus', description: 'a motor vehicle which is let out for hire without driver' },
    { id: 8, title: 'Hire Car', description: 'a motor vehicle which is let out for hire without driver' }
  ];

  $scope.select = function(use){
  	_.each($scope.uses, function(use){use.selected = false;});
  	use.selected = true;
  }

  $scope.continue = function(){
    	$location.path('vehicleinfo');
    }

})

.controller('VehicleInfoCtrl', function($rootScope, $scope, $location, $ionicScrollDelegate) {

	$rootScope.haskeypad = false;
	$scope.inputname = '';
	$scope.input = {};

	$scope.vehicleinfo = {
		year: '2010',
		make: 'Toyota',
		model: 'Land Cruiser',
		odometer: '12345678',
		colour: 'Red',
		chasisnumber: 'CX-123456789',
		enginenumber: '3C-123456'
	}

	$scope.toggleKeypad = function(){
		$rootScope.haskeypad = !$rootScope.haskeypad;
		
		if($rootScope.haskeypad){
			$ionicScrollDelegate.scrollBottom(true);
		} else {
			$ionicScrollDelegate.scrollTop(true);
		}
	}

	$scope.showAndSetKeypad = function(inputname, input, scroll){
		$rootScope.haskeypad = true;
		$scope.inputname = inputname;
		$scope.input = input;

		if(scroll){
			$ionicScrollDelegate.scrollTo(0, scroll, true);
		}
	}

	$scope.$on('changevalue', function (evt, value) {
        eval("$scope." + value[0] + " = '" + value[1] + "'");
    });

    $scope.$on('exit', function (evt, value) {
        $scope.toggleKeypad();
    });

	$scope.continue = function(){
    	$location.path('originalowner');
    }
})

.controller('OriginalOwnerCtrl', function($rootScope, $scope, $location, $ionicScrollDelegate) {

	$rootScope.haskeypad = false;
	$scope.inputname = '';
	$scope.input = {};

	$scope.originalowner = {
		driverslicence: 'XXX888',
		surname: 'Foley',
		givenname: 'Seth',
		residential: {
			unit: '',
			number: '',
			street: ''
		},
	}

	$scope.toggleKeypad = function(){
		$rootScope.haskeypad = !$rootScope.haskeypad;
		
		if($rootScope.haskeypad){
			$ionicScrollDelegate.scrollBottom(true);
		} else {
			$ionicScrollDelegate.scrollTop(true);
		}
	}

	$scope.showAndSetKeypad = function(inputname, input, scroll){
		$rootScope.haskeypad = true;
		$scope.inputname = inputname;
		$scope.input = input;

		if(scroll){
			$ionicScrollDelegate.scrollTo(0, scroll, true);
		}
	}

	$scope.$on('changevalue', function (evt, value) {
        eval("$scope." + value[0] + " = '" + value[1] + "'");
    });

    $scope.$on('exit', function (evt, value) {
        $scope.toggleKeypad();
    });

	$scope.continue = function(){
    	$location.path('privacystatement');
    }
})

.controller('PrivacyStatementCtrl', function($rootScope, $scope, $location) {
	$scope.continue = function(){
    	$location.path('payment');
    }
})

.controller('PaymentCtrl', function($rootScope, $scope, $location, $ionicModal, _, $timeout) {
	
	var timer;

	$scope.items = [
		{id: 0, title: 'XXXXXXXXXXXXXXXXX', rate: 88.88},
		{id: 1, title: 'XXXXXXXXXXXXXXXXX', rate: 88.88},
		{id: 2, title: 'XXXXXXXXXXXXXXXXX', rate: 88.88},
		{id: 3, title: 'XXXXXXXXXXXXXXXXX', rate: 88.88},
		{id: 4, title: 'XXXXXXXXXXXXXXXXX', rate: 88.88},
		{id: 5, title: 'XXXXXXXXXXXXXXXXX', rate: 88.88},
		{id: 6, title: 'XXXXXXXXXXXXXXXXX', rate: 88.88},
		{id: 7, title: 'XXXXXXXXXXXXXXXXX', rate: 88.88}
	]

	$scope.total = _.reduce($scope.items, function(memo, num){ return memo + num.rate; }, 0);

	$scope.payviacard = function(){

		html2canvas(document.querySelectorAll('.fadecontainer')[0], {
	        onrendered: function (canvas) {
	            document.querySelectorAll('#payviacard')[0].appendChild(canvas);
	        },
	        width: 1366,
	        height: 768
	    });

		$scope.modalcard.show();

		timer = $timeout(function(){
			$location.path('declaration')
		}, 5000);
	}

	$scope.payviamobile = function(){

		html2canvas(document.querySelectorAll('.fadecontainer')[0], {
	        onrendered: function (canvas) {
	            document.querySelectorAll('#payviamobile')[0].appendChild(canvas);
	        },
	        width: 1366,
	        height: 768
	    });

		$scope.modalmobile.show();

		timer = $timeout(function(){
			$location.path('declaration')
		}, 5000);
	}

	$ionicModal.fromTemplateUrl('templates/modal/payviacard.html', {
		scope: $scope,
		animation: 'slide-in-up'
		}).then(function(modal1) {
		$scope.modalcard = modal1;
	});

	$ionicModal.fromTemplateUrl('templates/modal/payviamobile.html', {
		scope: $scope,
		animation: 'slide-in-up'
		}).then(function(modal2) {
		$scope.modalmobile = modal2;
	});

	$scope.$on('modal.hidden', function() {

		var nodes = document.getElementsByTagName("canvas");

		for (var i = 0, len = nodes.length; i != len; ++i) {
		    nodes[0].parentNode.removeChild(nodes[0]);
		}

		$timeout.cancel( timer );
	});

	$scope.$on('$destroy', function() {
		$scope.modalcard.remove();
		$scope.modalmobile.remove();
	});


})

.controller('PayViaCardCtrl', function($rootScope, $scope, $location) {
})
.controller('PayViaMobileCtrl', function($rootScope, $scope, $location) {
})

.controller('DeclarationCtrl', function($rootScope, $scope, $location) {
	$scope.continue = function(){
    	$location.path('complete');
    }
})

.controller('CompleteCtrl', function($rootScope, $scope, $location) {
	$scope.okay = function(){
    	$location.path('home');
    }
    $scope.dosomethingelse = function(){
    	$location.path('home');
    }
})
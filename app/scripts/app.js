// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
'use strict';
angular.module('Servicensw', ['ionic', 'config', 'Servicensw.services', 'Servicensw.controllers', 'Servicensw.directives'])


.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

    // the pet tab has its own child nav-view and history
    .state('landingpage', {
      url: '/home',
      templateUrl: 'templates/landingpage.html',
      controller: 'IndexCtrl'
    })

    .state('licenceentry', {
      url: '/licenceentry',
      templateUrl: 'templates/licenceentry.html',
      controller: 'LicenceEntryCtrl'
    })

    .state('swipelicence', {
      url: '/swipelicence',
      templateUrl: 'templates/swipelicence.html',
      controller: 'SwipeLicenceCtrl'
    })

    .state('licenceentrykeypad', {
      url: '/licenceentrykeypad',
      templateUrl: 'templates/licenceentrykeypad.html',
      controller: 'LicenceEntryKeypadCtrl'
    })

    .state('platenumber', {
      url: '/platenumber',
      templateUrl: 'templates/platenumber.html',
      controller: 'PlateNumberCtrl'
    })

    .state('registerto', {
      url: '/registerto',
      templateUrl: 'templates/registerto.html',
      controller: 'RegisterToCtrl'
    })

    .state('customerdetails', {
      url: '/customerdetails',
      templateUrl: 'templates/customerdetails.html',
      controller: 'CustomerDetailsCtrl'
    })

    .state('dealerdetails', {
      url: '/dealerdetails',
      templateUrl: 'templates/dealerdetails.html',
      controller: 'DealerDetailsCtrl'
    })

    .state('corporationdetails', {
      url: '/corporationdetails',
      templateUrl: 'templates/corporationdetails.html',
      controller: 'CorporationDetailsCtrl'
    })

    .state('purchaseacquisition', {
      url: '/purchaseacquisition',
      templateUrl: 'templates/purchaseacquisition.html',
      controller: 'PurchaseAcquisitionCtrl'
    })

    .state('typeofuse', {
      url: '/typeofuse',
      templateUrl: 'templates/typeofuse.html',
      controller: 'TypeOfUseCtrl'
    })

    .state('vehicleinfo', {
      url: '/vehicleinfo',
      templateUrl: 'templates/vehicleinfo.html',
      controller: 'VehicleInfoCtrl'
    })

    .state('originalowner', {
      url: '/originalowner',
      templateUrl: 'templates/originalowner.html',
      controller: 'OriginalOwnerCtrl'
    })

    .state('privacystatement', {
      url: '/privacystatement',
      templateUrl: 'templates/privacystatement.html',
      controller: 'PrivacyStatementCtrl'
    })

    .state('payment', {
      url: '/payment',
      templateUrl: 'templates/payment.html',
      controller: 'PaymentCtrl'
    })

    .state('declaration', {
      url: '/declaration',
      templateUrl: 'templates/declaration.html',
      controller: 'DeclarationCtrl'
    })

    .state('complete', {
      url: '/complete',
      templateUrl: 'templates/complete.html',
      controller: 'CompleteCtrl'
    })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home');

});


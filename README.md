Service NSW Mockup
=========

Self service kiosk mockup for Service NSW

  - 19" Landscape Display (1366 x 768)


Version
----

X.X

Tech
-----------

Service NSW Mockup requirements

* [node.js] - evented I/O for the backend and npm
* [bower] - package manager `npm install -g bower`
* [grunt] - task runner `npm install -g grunt`
* [ionic] - Ionic Framework, front-end framework for developing hybrid mobile apps with HTML5, over AngularJS
* [ruby] - For compiling sass

Installation
--------------

```sh
git clone https://bitbucket.org/ernstmalifer/service-nsw-mockup.git servicensw
cd servicensw
npm install
bower install
grunt serve
```

License
----

engeges


**Free Software, Hell Yeah!**

[node.js]:http://nodejs.org
[bower]:http://bower.io/
[grunt]:http://gruntjs.com/
[ionic]:http://ionicframework.com/
[ruby]:https://www.ruby-lang.org/